<?php

use yii\db\Migration;

class m160409_095758_create_sign_type extends Migration
{
    public function up()
    {
        $this->createTable('sign_type', [
            'id' => $this->primaryKey() .' NOT NULL AUTO_INCREMENT',
            'type_name'=>$this->string(255).' NOT NULL'
        ]);
    }

    public function down()
    {
        $this->dropTable('sign_type');
    }
}
