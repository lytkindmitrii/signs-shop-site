<?php

use yii\db\Migration;

class m160409_113057_create_order_status extends Migration
{
    public function up()
    {
        $this->createTable('order_status', [
            'id' => $this->primaryKey().' NOT NULL AUTO_INCREMENT',
            'name'=>$this->string(255)
        ]);
    }

    public function down()
    {
        $this->dropTable('order_status');
    }
}
