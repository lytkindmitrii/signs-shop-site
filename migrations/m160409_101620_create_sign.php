<?php

use yii\db\Migration;

class m160409_101620_create_sign extends Migration
{
    public function up()
    {
        $this->createTable('sign', [
            'id_sign' => $this->primaryKey() .' NOT NULL AUTO_INCREMENT ',
            'num'=>$this->string(10),
            'name'=>$this->string(255),
            'description'=>$this->text(),
            'id_type'=>$this->integer(11).' NOT NULL',
            'price'=>$this->double(),
            'count'=>$this->integer()

        ]);
        $this->createIndex('idx-sign-id_type', 'sign', 'id_type');
        $this->addForeignKey('fk-type_id_type', 'sign', 'id_type', 'sign_type', 'id', 'CASCADE');

    }

    public function down()
    {
        $this->dropTable('sign');
    }
}
