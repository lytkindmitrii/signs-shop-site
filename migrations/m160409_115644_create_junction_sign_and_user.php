<?php

use yii\db\Migration;

class m160409_115644_create_junction_sign_and_user extends Migration
{
    public function up()
    {
        $this->createTable('order', [
            'id'=>$this->primaryKey(). ' NOT NULL AUTO_INCREMENT',
            'sign_id' => $this->integer(11) .' NOT NULL',
            'user_id' => $this->integer(11) .' NOT NULL',
            'status_id' => $this->integer(11) .' NOT NULL',
            'order_id' =>$this->integer() .' NOT NULL',
            'sign_count'=>$this->integer()



        ]);

        $this->createIndex('idx-order-user_id', 'order', 'user_id');
        $this->createIndex('idx-order-status_id', 'order', 'status_id');
        $this->createIndex('idx-order-sign_id', 'order', 'sign_id');

        $this->addForeignKey('fk-order-sign_id', 'order', 'sign_id', 'sign', 'id_sign', 'CASCADE');
        $this->addForeignKey('fk-order-user_id', 'order', 'user_id', 'user', 'id', 'CASCADE');
        $this->addForeignKey('fk-order-status_id', 'order', 'status_id', 'order_status', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('order');
    }
}
