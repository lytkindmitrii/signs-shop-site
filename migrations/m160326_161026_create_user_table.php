<?php

use yii\db\Migration;

class m160326_161026_create_user_table extends Migration
{
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey() . ' NOT NULL AUTO_INCREMENT',
            'email'=>$this->string(100) . ' NOT NULL',
            'password'=>$this->string(255) .'NOT NULL',
            'address'=>$this->string(255),
            'phone'=>$this->string(18)
        ]);
    }

    public function down()
    {
        $this->dropTable('user');
    }
}
