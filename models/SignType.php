<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sign_type".
 *
 * @property integer $id
 * @property string $type_name
 *
 * @property Sign[] $signs
 */
class SignType extends \yii\db\ActiveRecord 
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sign_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_name'], 'required'],
            [['type_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type_name' => 'Type Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSigns()
    {
        return $this->hasMany(Sign::className(), ['id_type' => 'id']);
    }
}
