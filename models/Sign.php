<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "sign".
 *
 * @property integer $id_sign
 * @property string $num
 * @property string $name
 * @property string $picture_path
 * @property string $description
 * @property integer $id_type
 * @property double $price
 * @property integer $count
 *
 * @property Order[] $orders
 * @property SignType $idType
 */
class Sign extends \yii\db\ActiveRecord
{   public $file;
    public $type_name;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sign';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['picture_path', 'id_type'], 'required'],
            [['description'], 'string'],
            [['id_type', 'count'], 'integer'],
            [['price'], 'number'],
            [['num'], 'string', 'max' => 10],
            [['name', 'picture_path'], 'string', 'max' => 255],
            [['file'], 'file', 'extensions' => 'png, jpg, gif']

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'num' => 'Рег. номер',
            'name' => 'Название',
            'picture_path' => 'Изображение',
            'description' => 'Описание',
            'id_type' => 'Тип',
            'price' => 'Цена(.pуб)',
            'count' => 'Количество',
            'type_name'=>'Тип'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['sign_id' => 'id_sign']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdType()
    {
        return $this->hasOne(SignType::className(), ['id' => 'id_type']);
    }
    public function getTypeName(){
        $signType = SignType::find()->where(['id'=>$this->id_type])->one();
        $this->type_name=$signType->getAttribute('type_name');

    }
    public function getSignById($id){
        $sign = Sign::find()->where(['id'=>$id]);
        return $sign;
    }
   
}
