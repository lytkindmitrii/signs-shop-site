<?php

namespace app\models;

use Yii;
use app\models\Sign;
/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property integer $sign_id
 * @property integer $user_id
 * @property integer $status_id
 * @property integer $order_id
 * @property integer $sign_count
 *
 * @property Sign $sign
 * @property OrderStatus $status
 * @property User $user
 */
class Order extends \yii\db\ActiveRecord
{
    public $sign;
    public $sign_name;
//    public $sign_pic;
//    public $sign_price;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sign_id', 'user_id'], 'required'],
            [['sign_id', 'user_id', 'status_id', 'order_id', 'sign_count'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sign_id' => 'Sign ID',
            'user_id' => 'User ID',
            'status_id' => 'Status ID',
            'order_id' => 'Order ID',
            'sign_count' => 'Sign Count',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSign()
    {  $this->sign = Sign::findOne(['id_sign' => 'sign_id']);
        return $this->hasOne(Sign::className(), ['id_sign' => 'sign_id']);
    }

    public function loadSign()
    {
         $this->sign = $this->getSign();
//        $this->sign_price = $this->sign->price;
//       $this->sign_pic = $this->sign->picture_path;

            
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(OrderStatus::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
