<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Sign;

/**
 * SignSearch represents the model behind the search form about `app\models\Sign`.
 */
class SignSearch extends Sign
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_sign', 'id_type', 'count'], 'integer'],
            [['num', 'name', 'picture_path', 'description'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Sign::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id_sign' => $this->id_sign,
            'id_type' => $this->id_type,
            'price' => $this->price,
            'count' => $this->count,
        ]);

        $query->andFilterWhere(['like', 'num', $this->num])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'picture_path', $this->picture_path])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
