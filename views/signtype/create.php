<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SignType */

$this->title = 'Create Sign Type';
$this->params['breadcrumbs'][] = ['label' => 'Sign Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sign-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
