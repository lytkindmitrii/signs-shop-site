<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SignTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sign Types';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sign-type-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Sign Type', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'type_name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
