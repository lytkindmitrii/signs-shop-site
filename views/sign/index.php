<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SignSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Signs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sign-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Sign', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'num',
            'name',
            'picture_path:image',
            'type_name',
            'description:ntext',
             'price',
             'count',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
