<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Sign */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Signs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sign-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_sign], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_sign], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'num',
            'name',
            'picture_path:image',
            'description:ntext',
            'id_type',
            'price',
            'count',
        ],
    ]) ?>

</div>
