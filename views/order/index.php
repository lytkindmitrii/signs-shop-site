<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $searchModel app\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Корзина';
$this->params['breadcrumbs'][] = $this->title;
?>



<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Номер',
                'value' => function($model) {
                    return $model->sign->num;
                }
            ],

            [
                'label' => 'Изображение',
                'value' => function($model) {
                    return $model->sign->picture_path;
                },
                'format' => 'image'

            ],

            [
                'label' => 'Знак',
                'value' => function($model) {
                    return $model->sign->name;
                }
            ],
            [
                'label' => 'Тип',
                'value' => function($model) {
                    $model->sign->getTypeName();
                    return $model->sign->type_name;
                }
            ],

            [
                'label' => 'Цена (руб)',
                'value' => function($model) {
                    return $model->sign->price.' руб.';
                }
            ],
            [
                'label' => 'Количество',
                'value' => function($model){
                    $i = Html::textInput('', $model->sign_count,['id'=>'num'.$model->sign_id, 'onchange'=>'updateCount('.$model->sign_id.');']);


                    return $i;
                },
                'format' => 'raw',

            ],

//            'sign_name',
//            'user_id',
//            'status_id',
//            'order_id',

        ],
    ]); ?>




    <div><h2 style="float:left;padding-right: 20px;"><?
        $orders = $dataProvider->getModels();
        $sum_price=0;
        foreach ($orders as $order){
            $sum_price+=$order->sign->price;
        }
      echo Html::encode('Сумма заказа: '.$sum_price.' руб.')
    ?></h2>
    <div style="padding-top:20px;"><?= Html::a('Купить', ['confirm'], ['class' => 'btn btn-success']) ?></div>
        </div>
</div>
<script>
    function updateCount(id_sign) {
        var count =  $('#num'+id_sign).val();
        console.log(count);
        $.ajax({
            type: "GET",
            url: "gri",
            data: "id_sign=" + id_sign+"&count="+count
//            success: function(result) {
//                alert('giii');
//            }
        });
    };

</script>