
<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\grid\GridView;
?>
<h1>Дорожные Знаки</h1>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'num',
        'name',
        'picture_path:image',
        'type_name',
        'description:ntext',
        'price',
        'count',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{link}',
            'buttons' => [
                'link' => function ($url,$model,$key) {

                    return Html::a('В корзину', ['add','key'=>$key], ['class'=>'btn btn-primary']);
                },
            ],
        ],
    ],
]); ?>
