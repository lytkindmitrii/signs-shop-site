<?php

namespace app\controllers;

use app\models\Order;
use app\models\Signup;
use Yii;
use yii\db\ActiveRecord;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\Login;
use app\models\Sign;
use yii\data\Pagination;

use app\models\SignSearch;


class SiteController extends Controller
{
    public function actionIndex(){
//        $query = Sign::find();
//
//        $pagination = new Pagination([
//            'defaultPageSize' => 5,
//            'totalCount' => $query->count(),
//        ]);
//
//        $signs = $query->orderBy('name')
//            ->offset($pagination->offset)
//            ->limit($pagination->limit)
//            ->all();
//
//        return $this->render('index', [
//            'signs' => $signs,
//            'pagination' => $pagination,
//        ]);
        $searchModel = new SignSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $models = $dataProvider->getModels();
        foreach ($models as $model) {
            $model->getTypeName();
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

        //return $this->render('index');
    }

    public function actionSignup(){
        $model = new Signup();
        if(isset($_POST['Signup'])){
//            var_dump($_POST['Signup']);
//            die();

            $model->attributes = Yii::$app->request->post('Signup');
            if($model->validate() && $model->signUp()) {
                return $this->goHome();
            }
        }
        return $this->render('signup', ['model'=>$model]);
    }

    public function actionLogin(){
        if(!Yii::$app->user->isGuest) return $this->goHome();

        $login_model = new Login();
        if(Yii::$app->request->post('Login')){
//            var_dump(Yii::$app->request->post('Login'));
//            die();
            $login_model->attributes = Yii::$app->request->post('Login');
            if($login_model->validate()){
                Yii::$app->user->login($login_model->getUser());
                return $this->goHome();
            }
        }
        return $this->render('login', ['login_model'=>$login_model]);
    }

    public function actionLogout(){
        if(!Yii::$app->user->isGuest){
            Yii::$app->user->logout();
            return $this->goHome();
        }
    }

    public function actionCart(){

    }
    public function actionAdd($key){
        $order = new Order();
        $order->setAttribute('sign_id',$key);
        $order->setAttribute('user_id',yii::$app->user->getId());
        $order->setAttribute('status_id',1);
        $order->setAttribute('order_id',12);
        $order->setAttribute('sign_count',1);

        $order->save();
        return $this->redirect(['index']);
    }
}

